#include "Kugel.h"

#define _USE_MATH_DEFINES
#include <cmath>


double Kugel::berechneVolumen()
{
	return 4. / 3. * M_PI * pow(_radius, 3);
}

double Kugel::berechneMasse()
{
	double vol = berechneVolumen();
	return _dichte * vol;
}
