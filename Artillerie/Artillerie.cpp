// Artillerie.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Kugel.h"
#include "Kanonenschuss.h"


void test_schuesse()
{
    //0,533 cm radius kugel
    //dichte eisen: 7.874 g/cm^3
    //energie: 1000 Joule

    Kugel eisenkugel(7.874e3, 0.533e-2);
    std::cout << "Masse der Kugel in [Gramm]: " << eisenkugel.berechneMasse() * 1000 << "\n";

    for (int i = 2; i < 90; i++)
    {
        double grad = 1.0 * i;

        Kanonenschuss schuss(grad, 1000, eisenkugel);

        std::cout << grad << " Grad - " << schuss.berechneAufschlagsdistanz() << " Meter\n";
    }
}



int main()
{
    test_schuesse();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
