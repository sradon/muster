#pragma once
#include "Kugel.h"
#include <cmath>

class Kanonenschuss
{
protected:
	double _abschusswinkel;
	double _energie;
	Kugel _kanonenkugel;

public:
	Kanonenschuss():
		_abschusswinkel(45), _energie(100), _kanonenkugel()
	{

	}

	Kanonenschuss(double abschusswinkel, double energie, const Kugel& kanonenkugel)
		:_abschusswinkel(abschusswinkel), _energie(energie), _kanonenkugel(kanonenkugel)
	{

	}

	const Kugel& get_kanonenkugel()
	{
		return _kanonenkugel;
	}

	void set_kanonenkugel(const Kugel& kugel)
	{
		_kanonenkugel = kugel;
	}

	double get_energie()
	{
		return _energie;
	}

	void set_energie(double energie)
	{
		_energie = energie;
	}

	double get_abschusswinkel()
	{
		return _abschusswinkel;
	}

	void set_abschusswinkel(double abschusswinkel)
	{
		_abschusswinkel = abschusswinkel;
	}

	double berechneAnfangsgeschwindigkeit();

	double berechneAufschlagsdistanz();

};

