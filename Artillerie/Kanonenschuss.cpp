#include "Kanonenschuss.h"

double Kanonenschuss::berechneAnfangsgeschwindigkeit()
{
    double masse = _kanonenkugel.berechneMasse();
    return sqrt(2 * _energie / masse);
}

double Kanonenschuss::berechneAufschlagsdistanz()
{
    const double g = 9.81;
    double velocity = berechneAnfangsgeschwindigkeit();
    double aufschlagdistanz = velocity * velocity / g * sin(2 * _abschusswinkel * M_PI / 180.0);
    return aufschlagdistanz;
}
