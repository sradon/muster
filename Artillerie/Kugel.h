#pragma once





class Kugel
{
protected:
	double _dichte;
	double _radius;

public:
	Kugel():
		_dichte(1.0), _radius(1.0)
	{

	}

	Kugel(double dichte, double radius) :
		_dichte(dichte), _radius(radius)
	{

	}


	double get_dichte()
	{
		return _dichte;
	}
	void set_dichte(double dichte)
	{
		_dichte = dichte;
	}
	double get_radius()
	{
		return _radius;
	}
	void set_radius(double radius)
	{
		_radius = radius;
	}

	double berechneVolumen();

	double berechneMasse();


};

